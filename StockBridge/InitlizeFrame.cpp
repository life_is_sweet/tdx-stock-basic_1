#include "pch.h"
#include "InitlizeFrame.h"
#include "BridgeBkWnd.h"
#include "Common/comdef.h"
#include "Common/Utils.h"
#include "resource.h"

BOOL WINAPI InstallBridgeUIFromParent(HWND hWnd);

using lpfnMdiWndProc = LRESULT(__thiscall*)(void*, int, WPARAM, LPARAM);

//global
BridgeBkWnd* wndBridgeBk = nullptr;

lpfnMdiWndProc pfnMdiWndProc;
ULONG pfnPrvMdiStockProc;
BOOL bIsStockWndReset = FALSE;
HWND hWndMdiClient = NULL;
HWND hwndMdiStock = NULL;

LRESULT WINAPI Detour_MdiStockWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_MDIDESTROY)
	{
		bIsStockWndReset = TRUE;
		if (wndBridgeBk)
			wndBridgeBk->ShowWindow(false, false);
	}

	return CallWindowProcA((WNDPROC)pfnPrvMdiStockProc,hWnd, uMsg, wParam, lParam);
}

LRESULT __fastcall Detour_MdiGridWndProc(void* ecx,void* edx, int uMsg, WPARAM wParam, LPARAM lParam)
{
	HWND hWnd = *utils::get_ptr<HWND*>(ecx, OFS_CWND_FRMHWND);
	
	
	if (!IsWindowVisible(hWnd))
		return pfnMdiWndProc(ecx, uMsg, wParam, lParam);;

	if (!hWndMdiClient || bIsStockWndReset)
	{
		if (bIsStockWndReset)
			bIsStockWndReset = FALSE;

		if (wndBridgeBk)
			wndBridgeBk->SetWndParent(hWnd);
		hwndMdiStock = GetParent(hWnd);
		hWndMdiClient = GetParent(hwndMdiStock);
		if (!pfnPrvMdiStockProc)
		{
			pfnPrvMdiStockProc = SetWindowLongA(hWndMdiClient, GWL_WNDPROC, (LONG)Detour_MdiStockWndProc);
		}
	}


	if (uMsg == WM_SYSKEYDOWN)
	{
		//HotKey Captured
		if ((wParam == (WPARAM)'F'))
		{
			InstallBridgeUIFromParent(hWnd);
			if (wndBridgeBk)
				wndBridgeBk->ShowStockBasic(ecx);
		}
		
		

	}
	else if (uMsg == WM_SETFOCUS || uMsg == WM_KILLFOCUS)
	{
		//detecting the mdi windows is next;
		HWND hChild = FindWindowExA(hWndMdiClient, NULL, NULL, NULL);
		if (hChild && wndBridgeBk)
		{
			wndBridgeBk->SetLastWndStatus(hChild == hwndMdiStock, uMsg == WM_SETFOCUS);
			wndBridgeBk->ShowStockBasic(ecx);
		}

	}
	else
	{
		if (wndBridgeBk)
			wndBridgeBk->ShowStockBasic(ecx);
	}
	
	LRESULT lres = pfnMdiWndProc(ecx, uMsg, wParam, lParam);
	
	return lres;
}

void Initlize(HMODULE hModule)
{
	HMODULE hInstance = hModule;
	CPaintManagerUI::SetInstance(hInstance);
	InitResource();
	MH_Initialize();

#if UI_ENABLE_ALL
	MH_CreateHook((void*)OFS_MDI_STOCK_WNDPROC, Detour_MdiGridWndProc, (void**)&pfnMdiWndProc);
	MH_EnableHook(MH_ALL_HOOKS);
#endif
	
}

BOOL WINAPI InstallBridgeUIFromParent(HWND hWnd)
{
	if (wndBridgeBk)
	{
		bool isVisible = IsWindowVisible(wndBridgeBk->GetHWND());
		wndBridgeBk->ShowWindow(!isVisible,false);
		wndBridgeBk->SetWndIsVisiable(!isVisible);
		return TRUE;
	}

	HRESULT Hr = ::CoInitialize(NULL);
	if (FAILED(Hr))
		return FALSE;

	wndBridgeBk = new BridgeBkWnd();
	if (wndBridgeBk == NULL)
		return FALSE;
	wndBridgeBk->SetWndParent(hWnd);
	wndBridgeBk->Create(hWnd, _T("TinyInfo"), UI_WNDSTYLE_DIALOG, WS_EX_NOACTIVATE, 0, 0, 0, 0);
	//wndBridgeBk->CenterWindow();
	wndBridgeBk->ShowWindow(true,false);

	//CPaintManagerUI::MessageLoop();

	::CoUninitialize();
	//PostQuitMessage(0);
	return TRUE;
}

//exported
BOOL WINAPI InstallBridgeUI()
{
	if (wndBridgeBk)
	{
		wndBridgeBk->ShowWindow(!IsWindowVisible(wndBridgeBk->GetHWND()));
		//if (!IsWindowVisible(wndBridgeBk->GetHWND()))
		return TRUE;
	}

	HRESULT Hr = ::CoInitialize(NULL);
	if (FAILED(Hr))
		return FALSE;

	wndBridgeBk = new BridgeBkWnd();
	if (wndBridgeBk == NULL)
		return FALSE;
	wndBridgeBk->Create(NULL, _T("TinyInfo"), UI_WNDSTYLE_DIALOG, /*WS_EX_NOACTIVATE*/0, 0, 0, 0, 0);
	//wndBridgeBk->CenterWindow();
	wndBridgeBk->ShowModal();
	
	CPaintManagerUI::MessageLoop();

	::CoUninitialize();
	PostQuitMessage(0);
	return TRUE;
}

//exported
void WINAPI UnInstallBridgeUI()
{
	if (wndBridgeBk)
	{
		SendMessage(wndBridgeBk->GetHWND(), WM_CLOSE, 0, 0);
		SendMessage(wndBridgeBk->GetHWND(), WM_DESTROY, 0, 0);
		SendMessage(wndBridgeBk->GetHWND(), WM_QUIT, 0, 0);
		//PostQuitMessage(0);
		
		wndBridgeBk = nullptr;
	}
	
	
}

void InitResource()
{
	// 资源类型
#ifdef _DEBUG
	//CPaintManagerUI::SetResourceType(UILIB_FILE);
	CPaintManagerUI::SetResourceType(UILIB_ZIPRESOURCE);
#else
	CPaintManagerUI::SetResourceType(UILIB_ZIPRESOURCE);
#endif
	// 资源路径
	CDuiString strResourcePath = CPaintManagerUI::GetInstancePath();
	// 加载资源
	switch (CPaintManagerUI::GetResourceType())
	{
	case UILIB_FILE:
	{
		strResourcePath += UI_SKIN_PATH;
		CPaintManagerUI::SetResourcePath(strResourcePath.GetData());
		// 加载资源管理器
		CResourceManager::GetInstance()->LoadResource(_T("res.xml"), NULL);
		break;
	}
	case UILIB_ZIPRESOURCE:
	{
		/*strResourcePath += UI_SKIN_PATH;
		CPaintManagerUI::SetResourcePath(strResourcePath.GetData());*/

		HRSRC hResource = ::FindResource(CPaintManagerUI::GetResourceDll(), MAKEINTRESOURCE(IDR_SKIN), _T("IDR_ZIPRES"));
		if (hResource != NULL) {
			DWORD dwSize = 0;
			HGLOBAL hGlobal = ::LoadResource(CPaintManagerUI::GetResourceDll(), hResource);
			if (hGlobal != NULL) {
				dwSize = ::SizeofResource(CPaintManagerUI::GetResourceDll(), hResource);
				if (dwSize > 0) {
					CPaintManagerUI::SetResourceZip((LPBYTE)::LockResource(hGlobal), dwSize);
					// 加载资源管理器
					CResourceManager::GetInstance()->LoadResource(_T("res.xml"), NULL);
				}
			}
			::FreeResource(hResource);
		}
	}
	break;
	}
}