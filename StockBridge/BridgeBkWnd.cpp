#include "pch.h"
#include "BridgeBkWnd.h"
#include "BridgeWnd.h"
#include "Common/comdef.h"

BridgeBkWnd::BridgeBkWnd()
{
	wndBasicInfo = nullptr;
	m_bIsWndVisiable = true;
	m_hWndParent = NULL;
}

BridgeBkWnd::~BridgeBkWnd()
{
}

void BridgeBkWnd::OnFinalMessage(HWND hWnd)
{
	__super::OnFinalMessage(hWnd); 
	delete this;
}

void BridgeBkWnd::InitWindow()
{

	//Create BasicInfo Window
	wndBasicInfo = new BridgeWnd(this);
	wndBasicInfo->Create(this->GetHWND(), _T("TinyInfo"), WS_OVERLAPPEDWINDOW | UI_WNDSTYLE_DIALOG, 0, 0, 0);
	
	OnInitlizeWndSize();
	wndBasicInfo->ShowWindow(true, false);
}

LRESULT BridgeBkWnd::HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;

	if (uMsg == WM_SETFOCUS || uMsg == WM_KILLFOCUS)
	{
		if(wndBasicInfo)
			wndBasicInfo->SendMessage(uMsg, wParam, lParam);
	}
	else if (uMsg == WM_WINDOWPOSCHANGING)
	{
		IsWndInParentWnd((WINDOWPOS*)lParam);
		
	}
	else if (uMsg == WM_MOVING)
	{
		RECT* rtMoving = (RECT*)lParam;
		if (!rtMoving || !IsWindowVisible(m_hWndParent))
			return 0;

		if (wndBasicInfo)
			::SetWindowPos(wndBasicInfo->GetHWND(), NULL, rtMoving->left, rtMoving->top, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	}
	else if (uMsg == WM_SHOWWINDOW)
	{
		if (wndBasicInfo)
			wndBasicInfo->ShowWindow(wParam, false);
	}

	return 0;
}

void BridgeBkWnd::OnInitlizeWndSize()
{
	
	if (!m_hWndParent)
		return;
	//set current rect
	RECT rtBasic;
	GetWindowRect(wndBasicInfo->GetHWND(), &rtBasic);
	int cx = rtBasic.right - rtBasic.left;
	int cy = rtBasic.bottom - rtBasic.top;
	
	//set position
	RECT rtMdi;
	GetWindowRect(m_hWndParent, &rtMdi);
	rtMdi.left += 50;
	rtMdi.top += 40;
	SetWindowPos(this->m_hWnd, FALSE, rtMdi.left, rtMdi.top, cx, cy, SWP_NOZORDER | SWP_NOACTIVATE);
	SetWindowPos(wndBasicInfo->GetHWND(), FALSE, rtMdi.left, rtMdi.top, 0, 0, SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOSIZE);
}

void BridgeBkWnd::IsWndInParentWnd(WINDOWPOS* pWndPos)
{
	
	RECT rtMdi;
	RECT rt;
	if (!pWndPos || !IsWindowVisible(m_hWndParent) || 
		pWndPos->flags & SWP_NOMOVE || !IsWindowVisible(this->m_hWnd))
		return;

	GetWindowRect(m_hWndParent, &rtMdi);
	GetClientRect(this->m_hWnd, &rt);

	if (pWndPos->x < rtMdi.left)
		pWndPos->x = rtMdi.left;
	if (pWndPos->y < rtMdi.top)
		pWndPos->y = rtMdi.top;

	
	if (pWndPos->x + rt.right > rtMdi.right)
		pWndPos->x = rtMdi.right - rt.right;
	if (pWndPos->y + rt.bottom + 20 > rtMdi.bottom)
		pWndPos->y = rtMdi.bottom - rt.bottom - 20;
	
}

void BridgeBkWnd::SetLastWndStatus(bool isStockWnd, bool isSetFoucs)
{
	bool bCurrentStatus = IsWindowVisible(this->m_hWnd);
	
	if (m_bIsWndVisiable)//show
	{

		if (isSetFoucs)
		{
			if (isStockWnd && !bCurrentStatus)
			{
				this->ShowWindow(true, false);
				
			}
		}
		else
		{
			if (!isStockWnd && bCurrentStatus)
			{
				this->ShowWindow(false, false);
			}
		}
	}
	
}

void BridgeBkWnd::ShowStockBasic(void* pStock)
{
	if (!pStock || !IsWindowVisible(this->m_hWnd))
		return;

	char* pStockClass = utils::get_ptr<char*>(pStock, OFS_CWND_STOCKPTR);
	if (pStockClass == nullptr || IsBadReadPtr(pStockClass, 4))
		return;

	pStockClass = *(char**)pStockClass;

	char* pStockCode = utils::read_ptr<char*>(pStockClass, OFS_CSTOCK_NAMEPTR);
	
	if (!pStockCode)
		return;

	enuStockMainType stockType = *utils::get_ptr<enuStockMainType*>(pStockClass, OFS_CSTOCK_TYPE);
	enuStockRegion stockRegion = *utils::get_ptr<enuStockRegion*>(pStockClass, OFS_CSTOCK_REGION);

	if (stockType == enuStockMainType::Normal && 
		(stockRegion == enuStockRegion::ShangHai || stockRegion == enuStockRegion::ShenZhen))
	{
		if (wndBasicInfo)
			wndBasicInfo->OnPraseByStockCode(pStockCode);
	}
	
	

}
