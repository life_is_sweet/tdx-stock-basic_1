#pragma once
#include <unordered_map>
#include <thread>
#include <mutex>
class BridgeWnd:public WindowImplBase
{
public:
	BridgeWnd(WindowImplBase* hParent);
	~BridgeWnd();
	virtual void OnFinalMessage(HWND hWnd) { __super::OnFinalMessage(hWnd); delete this; };
	virtual CDuiString GetSkinFile() { return _T("basicdlg.xml"); };
	virtual LPCTSTR GetWindowClassName(void) const { return _T("supertape_dlg"); };
	virtual void InitWindow();

	LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	DUI_DECLARE_MESSAGE_MAP()
	virtual void OnClick(TNotifyUI& msg);

	//设置所有关联窗口的大小
	void OnSetFrameSize();

	//设置窗口是否被激活状态
	void OnSetWndActive(bool isActive);

	//捕获股票信息接口
	void OnPraseByStockCode(const char* stockCode);

	//设置股票信息
	void OnSetBasicInfo();


	int ParseRoutine();
private:
	friend class BridgeBkWnd;
	friend class BasicParse;
	void OnReSetLabelUI();
	
public:
	
private:
	bool m_wndDraggable;

	BridgeBkWnd* m_hParent;


	CControlUI* m_frmMain;
	CControlUI* m_frmToolbar;

	//Ui Text Controller
	std::unordered_map<std::string, std::tuple<CLabelUI* ,LPCTSTR>> m_labelUi;

	//<显示名称，提示>
	std::unordered_map<std::string, std::tuple<std::string, std::string>> m_uiBasic;

	//当前股票代码
	std::string m_stockCode;

	std::mutex mtx;
};

