#define WIN32_LEAN_AND_MEAN
#include <Windows.h>  
#include <tchar.h>

#define IDB_BTN_LOAD		3301
#define IDB_BTN_UNLOAD		3302

#pragma comment(lib,"..\\bin\\StockBridge.lib")

__declspec(dllimport) BOOL WINAPI InstallBridgeUI();
__declspec(dllimport) void WINAPI UnInstallBridgeUI();

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CenterWindow(HWND hwnd);


int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,_In_opt_ HINSTANCE hPrevInstance,_In_ LPTSTR    lpCmdLine,_In_ int       nCmdShow)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = nullptr;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = _T("AppClassName");
	wcex.hIconSm = nullptr;
	
	RegisterClassEx(&wcex);


	HWND hwnd = CreateWindowEx(WS_EX_WINDOWEDGE,
		_T("AppClassName"),
		_T("LoadTestApp"),
		WS_OVERLAPPEDWINDOW,
		0,
		0,
		350,
		400,
		NULL,
		NULL,
		hInstance,
		NULL);
	if (hwnd == NULL)
	{
		return -1;
	}
	
	CenterWindow(hwnd);
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
	
}

void CenterWindow(HWND hwnd)
{
	
	RECT rcDlg = { 0 };
	::GetWindowRect(hwnd, &rcDlg);
	RECT rcArea = { 0 };
	RECT rcCenter = { 0 };
	HWND hWnd = hwnd;
	HWND hWndParent = ::GetParent(hwnd);
	HWND hWndCenter = ::GetWindow(hwnd, GW_OWNER);
	if (hWndCenter != NULL)
		hWnd = hWndCenter;

	// 处理多显示器模式下屏幕居中
	MONITORINFO oMonitor = {};
	oMonitor.cbSize = sizeof(oMonitor);
	::GetMonitorInfo(::MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST), &oMonitor);
	rcArea = oMonitor.rcWork;

	if (hWndCenter == NULL)
		rcCenter = rcArea;
	else
		::GetWindowRect(hWndCenter, &rcCenter);

	int DlgWidth = rcDlg.right - rcDlg.left;
	int DlgHeight = rcDlg.bottom - rcDlg.top;

	// Find dialog's upper left based on rcCenter
	int xLeft = (rcCenter.left + rcCenter.right) / 2 - DlgWidth / 2;
	int yTop = (rcCenter.top + rcCenter.bottom) / 2 - DlgHeight / 2;

	// The dialog is outside the screen, move it inside
	if (xLeft < rcArea.left) xLeft = rcArea.left;
	else if (xLeft + DlgWidth > rcArea.right) xLeft = rcArea.right - DlgWidth;
	if (yTop < rcArea.top) yTop = rcArea.top;
	else if (yTop + DlgHeight > rcArea.bottom) yTop = rcArea.bottom - DlgHeight;
	::SetWindowPos(hwnd, NULL, xLeft, yTop, -1, -1, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_DESTROY:
		//UnInstallBridgeUI();
		PostQuitMessage(0);
		
		return 0;

	case WM_CREATE:
	{
		CreateWindowW(L"Button", L"加载UI", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
			35, 10, 80, 40, hWnd, (HMENU)IDB_BTN_LOAD, nullptr, NULL);
		CreateWindowW(L"Button", L"卸载UI", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
			150, 10, 80, 40, hWnd, (HMENU)IDB_BTN_UNLOAD, nullptr, NULL);
	}
	break;



	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDB_BTN_LOAD:
			InstallBridgeUI();
			
			break;
		case IDB_BTN_UNLOAD:
			UnInstallBridgeUI();
			break;
		default:
			break;
		}
	}

	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}